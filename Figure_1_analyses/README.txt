## Files for the production of divergence times under the different parameters included in Fig. 1. Betts et al., 2018 ##

Each file contains the layout of the PAML control files as well as the calibration/tree file and the data files used for the analysis. 
