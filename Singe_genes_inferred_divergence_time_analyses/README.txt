# This directory contains all the files needed to complete a divergence dating analysis on separate gene.

# The mcmctree control files are provided as is an example of a tmp.ctl file once altered to have the right information.

# The calibration files and sequence data are included for each gene.
